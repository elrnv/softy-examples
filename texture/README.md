All `.hdr` files in this folder are downloaded from [HDRI Haven](https://hdrihaven.com) and are
licensed under [CC0 public domain license](https://creativecommons.org/publicdomain/zero/1.0/).
