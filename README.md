# Softy Examples

This repository contains simulation examples of various contact scenarios involving soft tissues,
rigid bodies and cloth.

To run the examples simply run `make` from the `hip` subdirectory for a list of supported targets,
or run `make working` to run all targets known to be working. It is assumed that you have `python`,
 `make`, Houdini and the [Softy Houdini plugin](github.com/elrnv/softy.git) installed on your
machine.

These examples were tested on Linux machines only, although they should also work on macOS.
