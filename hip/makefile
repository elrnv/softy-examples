MAKE_PID := $(shell echo $$PID)
JOB_FLAG := $(filter -j%, $(subst -j ,-j,$(shell ps T | grep "^\s*$(MAKE_PID).*$(MAKE)")))
JOBS     := $(subst -j,,$(JOB_FLAG))
UNAME_S  := $(shell uname -s)
ifeq ($(JOBS),)
	ifeq ($(UNAME_S),Linux)
		JOBS = $(shell nproc --all)
	else ifeq ($(UNAME_S),Darwin)
		JOBS = $(shell sysctl -n hw.ncpu)
	endif
endif

HXX=hython -j $(JOBS)
HXXRUN=$(HXX) run.py
LOG=2>&1 | tee

usage:
	@echo "Available jobs: " $(JOBS)
	@echo "To reproduce simulations in our 2023 paper please choose one of the following targets:"
	@echo "  ball_bounce 				(Experiments involving a bouncing ball)"
	@echo "  box_slide_error			(Experiments for a box sliding down a ramp)"
	@echo "  ball_in_a_box  			(A ball with an initial spin is trapped in a box with elastic walls)"
	@echo "  tire 						(A soft inflated tire rubs against a surface as it spins)"
	@echo ""
	@echo "To reproduce simulations in our SIGGRAPH 2021 paper please choose one of the following targets:"
	@echo "  block_slide      			(Sliding blocks at 10 degree include with coefficients 0.176, 0.177)"
	@echo "  block_slide_horizontal     (A block is slid back and forth demonstrating friction forwarding)"
	@echo "  armadillo_fit    			(Armadillo tanktop dynamic fitting simulation)"
	@echo "  tube_slide       			(Cloth sliding across a tube)"
	@echo "  belt             			(Belt drive example)"
	@echo "  ball             			(A ball bounces on an inclined surface)"
	@echo "  stool            			(A stool sliding down a ramp demonstrating stick-slip chatter)"
	@echo "  inclined_plane            	(A square patch of cloth is dropped onto a flat inclined plane surface)"

# Working examples
working: block_slide tube_slide armadillo_fit belt ball stool

# Experiments
ball_bounce:
	$(HXX) ball_height_experiment.py --subdir ball_bounce ball_bounce.hipnc /obj/softy/HEIGHT /obj/softy/softy_solver

box_slide_error:
	$(HXXRUN) --subdir box_slide box.hipnc 60 /obj/box_slide/OUT $(LOG) ./log/box_slide_error.log

# Simulation targets
box_slide:
	$(HXXRUN) --subdir box_slide box_slide.hipnc 10 /obj/geo/OUT $(LOG) ./log/box_slide.log
	gltfgen -r -f 50 ./gltf/box_slide.glb "./out/box_slide/box_slide-{*}_#.vtk"

tire:
	$(HXXRUN) --subdir tire tire_v2.hipnc 3200 /obj/softy/OUT $(LOG) ./log/tire.log
	gltfgen -r -f 1600 ./gltf/tire.glb "./out/tire/{tire}-softy-OUT_#.vtk"

ball_in_a_box: ball_in_a_box_backtracking ball_in_a_box_contact ball_in_a_box_assisted ball_in_a_box_backtracking_adapt ball_in_a_box_contact_adapt ball_in_a_box_assisted_adapt ball_in_a_box_backtracking_lo ball_in_a_box_contact_lo ball_in_a_box_assisted_lo ball_in_a_box_backtracking_adapt_lo ball_in_a_box_contact_adapt_lo ball_in_a_box_assisted_adapt_lo

ball_in_a_box_backtracking:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/backtracking
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_backtracking.glb "./out/ball_in_a_box/ball_spin_in_box-softy-backtracking{*}_#.vtk"

ball_in_a_box_contact:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/contact_only
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_contact.glb "./out/ball_in_a_box/ball_spin_in_box-softy-contact_only{*}_#.vtk"

ball_in_a_box_assisted:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/assisted
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_assisted.glb "./out/ball_in_a_box/ball_spin_in_box-softy-assisted{*}_#.vtk"

ball_in_a_box_backtracking_adapt:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/backtracking_adapt
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_backtracking_adapt.glb "./out/ball_in_a_box/ball_spin_in_box-softy-backtracking_adapt{*}_#.vtk"

ball_in_a_box_contact_adapt:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/contact_only_adapt
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_contact_adapt.glb "./out/ball_in_a_box/ball_spin_in_box-softy-contact_only_adapt{*}_#.vtk"

ball_in_a_box_assisted_adapt:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/assisted_adapt
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_assisted_adapt.glb "./out/ball_in_a_box/ball_spin_in_box-softy-assisted_adapt{*}_#.vtk"

ball_in_a_box_backtracking_lo:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/backtracking_lo
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_backtracking_lo.glb "./out/ball_in_a_box/ball_spin_in_box-softy-backtracking_lo{*}_#.vtk"

ball_in_a_box_contact_lo:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/contact_only_lo
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_contact_lo.glb "./out/ball_in_a_box/ball_spin_in_box-softy-contact_only_lo{*}_#.vtk"

ball_in_a_box_assisted_lo:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/assisted_lo
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_assisted_lo.glb "./out/ball_in_a_box/ball_spin_in_box-softy-assisted_lo{*}_#.vtk"

ball_in_a_box_backtracking_adapt_lo:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/backtracking_adapt_lo
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_backtracking_adapt_lo.glb "./out/ball_in_a_box/ball_spin_in_box-softy-backtracking_adapt_lo{*}_#.vtk"

ball_in_a_box_contact_adapt_lo:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/contact_only_adapt_lo
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_contact_adapt_lo.glb "./out/ball_in_a_box/ball_spin_in_box-softy-contact_only_adapt_lo{*}_#.vtk"

ball_in_a_box_assisted_adapt_lo:
	$(HXXRUN) --subdir ball_in_a_box ball_spin_in_box.hipnc 800 /obj/softy/assisted_adapt_lo
	gltfgen -r -f 1600 ./gltf/ball_in_a_box_assisted_adapt_lo.glb "./out/ball_in_a_box/ball_spin_in_box-softy-assisted_adapt_lo{*}_#.vtk"




# Old SIGGRAPH 2021 targets
block_slide:
	$(HXXRUN) --subdir block_slide block_slide.hipnc 5000 /obj/softy/OUT_STICK /obj/softy/OUT_SLIP /obj/houdini/OUT /obj/ramp/OUT $(LOG) ./log/block_slide.log
	gltfgen -r -f 10 ./gltf/block_slide.glb "./out/block_slide/block_slide-{*}_#.vtk"

block_slide_horizontal:
	$(HXXRUN) --subdir block_slide_horizontal block_slide_horizontal.hipnc 160 /obj/sim/OUT_BLOCK /obj/sim/OUT_BLOCK_NOFF $(LOG) ./log/block_slide_horizontal.log
	gltfgen -r -f 200 ./gltf/block_slide_horizontal.glb "./out/block_slide_horizontal/block_slide_horizontal-{*}_#.vtk"

tube_slide: tube_slide_basic tube_slide_resolution tube_slide_cof tube_slide_overlay

# Compare different overlay offsets.
tube_slide_overlay:
	$(HXXRUN) --subdir tube_slide_overlay tube_slide_overlay.hipnc 500 /obj/softy_1/OUT_CLOTH /obj/softy_2/OUT_CLOTH /obj/softy_3/OUT_CLOTH /obj/softy_4/OUT_CLOTH /obj/softy_5/OUT_CLOTH $(LOG) ./log/tube_slide_overlay.log
	gltfgen -r -f 100 ./gltf/tube_slide_overlay.glb "./out/tube_slide_overlay/tube_slide_overlay-{*}_#.vtk"

# Compare coefficient of friction differences.
tube_slide_cof:
	$(HXXRUN) --subdir tube_slide_cof tube_slide_cof.hipnc 500 /obj/softy_120/OUT_CLOTH /obj/softy_130/OUT_CLOTH /obj/softy_140/OUT_CLOTH /obj/softy_150/OUT_CLOTH /obj/softy_160/OUT_CLOTH $(LOG) ./log/tube_slide_cof.log
	gltfgen -r -f 100 ./gltf/tube_slide_cof.glb "./out/tube_slide_cof/tube_slide_cof-{*}_#.vtk"

# Compare simulation fidelity of a piece of cloth sliding across a tube with different resolutions
# but same resolution cloth.
tube_slide_houdini_basic:
	$(HXXRUN) --subdir tube_slide_houdini_basic tube_slide_basic.hipnc 500 /obj/coarse_tube/OUT /obj/coarse_cloth/OUT /obj/medium_tube/OUT /obj/medium_cloth/OUT /obj/fine_tube/OUT /obj/fine_cloth/OUT $(LOG) ./log/tube_slide_basic.log
	gltfgen -r -f 100 ./gltf/tube_slide_houdini_basic.glb "./out/tube_slide_houdini_basic/tube_slide_basic-{*}_#.vtk"

tube_slide_softy_basic:
	$(HXXRUN) --subdir tube_slide_softy_basic tube_slide_resolution.hipnc 100 /obj/softy_basic/OUT_CLOTH $(LOG) ./log/tube_slide_softy_basic.log
	gltfgen -r -f 50 ./gltf/tube_slide_softy_basic.glb "./out/tube_slide_softy_basic/tube_slide_resolution-{*}_#.vtk"

# Compare simulation fidelity of a piece of cloth sliding across a tube with different resolutions.
tube_slide_resolution:
	$(HXXRUN) --subdir tube_slide_resolution tube_slide_resolution.hipnc 500 /obj/coarse_tube/OUT /obj/coarse_cloth/OUT /obj/medium_tube/OUT /obj/medium_cloth/OUT /obj/fine_tube/OUT /obj/fine_cloth/OUT /obj/softy_coarse/OUT_TUBE /obj/softy_coarse/OUT_CLOTH /obj/softy_medium/OUT_TUBE /obj/softy_medium/OUT_CLOTH /obj/softy_fine/OUT_TUBE /obj/softy_fine/OUT_CLOTH $(LOG) ./log/tube_slide_resolution.log
	gltfgen -r -f 100 ./gltf/tube_slide_resolution.glb "./out/tube_slide_resolution/tube_slide_resolution-{*}_#.vtk"

armadillo_fit:
	$(HXXRUN) --subdir armadillo_rigid_fit armadillo_fit.hipnc 1000 /obj/sim/rigid_frictionless_fit_cloth /obj/sim/rigid_fit_cloth /obj/sim/body_animation $(LOG) ./log/armadillo_rigid_fit.log
	gltfgen -s 2 -r -f 250 armadillo_rigid_fit.glb "./out/armadillo_rigid_fit/armadillo_fit-sim-{*}_#.vtk"
	$(HXXRUN) --subdir armadillo_soft_fit armadillo_fit.hipnc 1000 /obj/sim/soft_frictionless_fit_cloth /obj/sim/soft_frictionless_fit_body /obj/sim/soft_fit_cloth /obj/sim/soft_fit_body $(LOG) ./log/armadillo_soft_fit.log
	gltfgen -s 2 -r -f 250 ./gltf/armadillo_soft_fit.glb "./out/armadillo_soft_fit/armadillo_fit-sim-{*}_#.vtk"

belt:
	$(HXXRUN) --subdir belt_2 belt.hipnc 1250 /obj/sim/DRIVER_2 /obj/sim/BELT_2 /obj/sim/DRIVEN_2 $(LOG) ./log/belt_2.log
	$(HXXRUN) --subdir belt_02 belt.hipnc 1250 /obj/sim/DRIVER_02 /obj/sim/BELT_02 /obj/sim/DRIVEN_02 $(LOG) ./log/belt_02.log
	gltfgen -u '{"ref_fv": f32}' -s 2 -r -f 200 ./gltf/belt.glb "./out/belt_*/belt-sim-{*}_#.vtk"

ball:
	$(HXXRUN) --subdir ball_01 ball.hipnc 750 /obj/softy/OUT_01 /obj/ramp/OUT $(LOG) ./log/ball_01.log
	$(HXXRUN) --subdir ball_05 ball.hipnc 750 /obj/softy/OUT_05 $(LOG) ./log/ball_05.log
	$(HXXRUN) --subdir ball_1 ball.hipnc 750 /obj/softy/OUT_1 $(LOG) ./log/ball_1.log
	gltfgen -r -f 100 ./gltf/ball.glb "./out/ball_*/ball-{*}_#.vtk"

stool:
	$(HXXRUN) --subdir stool stool.hipnc 3000 /obj/sim/OUT_STOOL $(LOG) ./log/stool.log
	gltfgen -s 2 -r -f 500 ./gltf/stool.glb "./out/stool/stool-sim-{*}_#.vtk"

inclined_plane_fine_176:
	$(HXXRUN) --subdir inclined_plane inclined_plane.hipnc 2000 /obj/sim/fine_176 $(LOG) ./log/inclined_plane_fine_176.log
inclined_plane_fine_177:
	$(HXXRUN) --subdir inclined_plane inclined_plane.hipnc 2000 /obj/sim/fine_177 $(LOG) ./log/inclined_plane_fine_177.log

inclined_plane_coarse_176:
	$(HXXRUN) --subdir inclined_plane inclined_plane.hipnc 2000 /obj/sim/coarse_176 $(LOG) ./log/inclined_plane_coarse_176.log
inclined_plane_coarse_177:
	$(HXXRUN) --subdir inclined_plane inclined_plane.hipnc 2000 /obj/sim/coarse_177 $(LOG) ./log/inclined_plane_coarse_177.log

inclined_plane_medium_176:
	$(HXXRUN) --subdir inclined_plane inclined_plane.hipnc 2000 /obj/sim/medium_176 $(LOG) ./log/inclined_plane_medium_176.log
inclined_plane_medium_177:
	$(HXXRUN) --subdir inclined_plane inclined_plane.hipnc 2000 /obj/sim/medium_177 $(LOG) ./log/inclined_plane_medium_177.log

inclined_plane: inclined_plane_fine_177 inclined_plane_medium_177 inclined_plane_coarse_177 inclined_plane_fine_176 inclined_plane_medium_176 inclined_plane_coarse_176
	gltfgen -r -f 25 ./gltf/inclined_plane.glb "./out/inclined_plane/inclined_plane-sim-{*}_#.vtk"


ball_on_trampoline_lo:
	$(HXXRUN) --subdir ball_on_trampoline_lo ball_on_trampoline.hipnc 1500 /obj/sim/LO_TRAMP /obj/sim/LO_BALL $(LOG) ./log/ball_on_trampoline_lo.log
ball_on_trampoline_hi:
	$(HXXRUN) --subdir ball_on_trampoline_hi ball_on_trampoline.hipnc 1500 /obj/sim/HI_TRAMP /obj/sim/HI_BALL $(LOG) ./log/ball_on_trampoline_hi.log

ball_on_trampoline: ball_on_trampoline_lo ball_on_trampoline_hi
	gltfgen -r -f 100 ./gltf/ball_on_trampoline.glb "./out/ball_on_trampoline_*/ball_on_trampoline-sim-{*}_#.vtk"
