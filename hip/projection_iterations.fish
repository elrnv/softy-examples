#!/usr/bin/env fish

set lines (rg "Friction iteration count:" $argv[1])

set data (echo -n "a = ["
for line in $lines
    set parts (string split ":" $line)
    echo -n "$parts[-1], "
end
echo -n "]")

mkdir -p /tmp/count_friction_iterations/
echo $data > /tmp/count_friction_iterations/data.jl

echo "Maximum projection iterations:"
julia -E "using LinearAlgebra;
    include(\"/tmp/count_friction_iterations/data.jl\");
    maximum(a)"

echo "Average projection iterations:"
julia -E "using LinearAlgebra, Statistics;
    include(\"/tmp/count_friction_iterations/data.jl\");
    mean(a)"
