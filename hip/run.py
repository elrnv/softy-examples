"""
Run simulation examples in this folder
"""

import hou, sys
import argparse
from os.path import basename, splitext, dirname, exists
from os import makedirs

parser = argparse.ArgumentParser()
parser.add_argument("hip_file", help="Hip file to load, which contains the desired node to be run.")
parser.add_argument("num_frames", type=int, help="Number of frames to evaluate.")
parser.add_argument("--subdir", required=False, help="Output subdirectory.")
parser.add_argument("--nohperf", help="Do not collect performance stats.", action="store_true")
parser.add_argument("file_node_paths", metavar='file_node_path', nargs='+',
                    help="Absolute node path of an output file node.")
args = parser.parse_args()

# Load the hip file
hipfile = args.hip_file
hou.hipFile.load(hipfile, ignore_load_warnings=True)
projectname = splitext(basename(hipfile))[0]

# Determine and create the output directory including a subdir if necessary
outpath = "out"
if args.subdir != None:
    outpath += "/" + args.subdir
if not exists(outpath):
    makedirs(outpath)

# Evaluate the node at the given frame and save the resulting geometry
def eval(i, node, outfile):
    geo = node.geometryAtFrame(i)
    geo.saveToFile(outpath + "/" + outfile + "_" + str(i) + ".vtk")

# Evaluate the assigned nodes at the specified range.
def run(args):
    # Iterate over all the frames, then the nodes. This ensures that performance stats measure the
    # right thing since the one solve result can be used for different nodes, and it will be
    # effectively reused if all nodes are processed at the same frame.
    for i in range(1, args.num_frames):
        for nodepath in args.file_node_paths:
            node = hou.node(nodepath)
            if node is None:
                print("Couldn't locate the file node at %s" % nodepath)
                parser.print_help()
                sys.exit(2)

            # Build an output file path used to save geometry
            nodefilepath = nodepath.replace("/obj/", "/").replace("/","-")
            outfile = projectname + nodefilepath

            eval(i, node, outfile)


# If we don't need to generate performance stats, just evaluate the nodes
if args.nohperf:
    run(args)

# Otherwise collect all the needed stats, then run the nodes and finally save the performance stats
else:
    statspath = "stats"
    if not exists(statspath):
        makedirs(statspath)

    # Setup performance monitor
    opts = hou.PerfMonRecordOptions(
            cook_stats = True,
            solve_stats = False,
            draw_stats = False,
            gpu_draw_stats = False,
            viewport_stats = False,
            script_stats = False,
            render_stats = False,
            thread_stats = False,
            frame_stats = False,
            memory_stats = True,
            errors = True)

    if args.subdir != None:
        statsfile = args.subdir
    else:
        statsfile = projectname

    profile = hou.perfMon.startProfile(statsfile, opts)

    run(args)

    profile.stop()

    hperfpath = statspath + "/" + statsfile + ".hperf"

    print("Saving hperf to " + hperfpath)
    profile.save(hperfpath)
