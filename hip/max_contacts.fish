#!/usr/bin/env fish

set counts (rg -U "Total number of inequality constraints...............:" $argv[1] | \
            sd 'Total number of inequality constraints...............: *(...)' '$1')

            julia -E "a = [$counts]; findmax(a[1:end])"
