#!/usr/bin/env fish

set joined (rg -U "Maximum contact impulse.*\n(.*Solving Friction\n.*\n.*\n.*\n)+.*softy::fem::" $argv[1] | \
            sd '.*Solving Friction\n.*\n.*\n.*\n' '%' | \
            sd '\[.*\] Maximum contact impulse.*\n%' '%' | \
            sd '%\[.*softy::fem::.*' '%' | \
            sd ' ' '\n')
set lengths (
echo "a = ["
for line in $joined
    set len (string length $line)
    echo -n "$len, "
end
echo "]")

echo "Maximum friction iterations:"
julia -E "$lengths; sort(a, rev=true)[1:20]"
julia -E "$lengths; findmax(a)"

echo "Average converged friction iterations:"
julia -E "$lengths; using Statistics; mean(filter(x -> x != 20, a))"
