#!/usr/bin/env fish

set joined (rg -U "Maximum contact impulse.*\n(.*Solving Friction\n.*\n.*\n.*\n)+.*softy::fem::" $argv[1] | \
            sd '.*Solving Friction\n.*\n.*\n.*\n' '%' | \
            sd '\[.*\] Maximum contact impulse.*\n%' '%' | \
            sd '%\[.*softy::fem::.*' '%' | \
            sd ' ' '\n')
for line in $joined
    set len (string length $line)
    echo -n "$len "
end
echo ""

