"""
Run ball bounce experiments in this folder
"""

import hou, sys
import argparse
from os.path import basename, splitext, dirname, exists
from os import makedirs

parser = argparse.ArgumentParser()
parser.add_argument("hip_file", help="Hip file to load, which contains the desired node to be run.")
parser.add_argument("--subdir", required=False, help="Output subdirectory.")
parser.add_argument("--nohperf", help="Do not collect performance stats.", action="store_true")
parser.add_argument("file_node_path", help="Absolute node path of an output file node.")
parser.add_argument("solver_node_path", help="Absolute node path of the solver node.")
args = parser.parse_args()

# Load the hip file
hipfile = args.hip_file
hou.hipFile.load(hipfile, ignore_load_warnings=True)
projectname = splitext(basename(hipfile))[0]

# Config matrix
#fps_l = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
fps_l = [200, 400, 800, 1600, 3200, 6400, 12800, 25600]
#fps_l = [1600, 3200, 6400, 12800, 25600]
#integrator_l = ['be', 'bdf2', 'trbdf2', 'sdirk2', 'trbdf2u']
integrator_l = ['be', 'bdf2', 'sdirk2']
damping_l = [2.5]
friction_l = [0]
#num_iterations_l = [1,200]
num_iterations_l = [1]
zone_pressurization_l = [1, 2]

def experiment_dir(fps, integrator, damping, num_iterations, pressurization):
    return integrator + "_f" + str(fps) + "_d" + str(damping) + "_n" + str(num_iterations) + "_p" + str(pressurization)

def experiment_lhs(integrator, fps_i, damping_i, num_iterations_i, friction_i, pressurization_i):
    return integrator + "[" + str(fps_i) + "][" + str(damping_i) + "][" + str(num_iterations_i) + "][" + str(friction_i) + "][" + str(pressurization_i) + "]"

# Determine and create the output directory including a subdir if necessary
outpath = "out"
if args.subdir != None:
    outpath += "/" + args.subdir
if not exists(outpath):
    makedirs(outpath)

# Evaluate the node at the given frame and save the resulting geometry
def eval(i, node):
    geo = node.geometryAtFrame(i)
    height = 0.0
    if geo is not None:
        height = geo.floatAttribValue("height")
    #geo.saveToFile(outpath + "/" + outfile + "_" + str(i) + ".vtk")
    return height

# Evaluate the assigned nodes at the specified range.
def run(args):
    nodepath = args.file_node_path
    node = hou.node(nodepath)
    if node is None:
        print("Couldn't locate the file node at %s" % nodepath)
        parser.print_help()
        sys.exit(2)

    solvernodepath = args.solver_node_path
    solvernode = hou.node(solvernodepath)
    if solvernode is None:
        print("Couldn't locate the solver node at %s" % solvernodepath)
        parser.print_help()
        sys.exit(2)

    results = ""
    # Iterate over all configs
    for integrator in integrator_l:
        for [fps_i, fps] in enumerate(fps_l):
            num_frames = fps # Simulate for 1 second
            for [damping_i, damping] in enumerate(damping_l):
                for [friction_i, friction] in enumerate(friction_l):
                    for [num_iterations_i, num_iterations] in enumerate(num_iterations_l):
                        for [pressurization_i, pressurization] in enumerate(zone_pressurization_l):
                            heights = []
                            exp_lhs = experiment_lhs(integrator, fps_i, damping_i, num_iterations_i, friction_i, pressurization_i)
                            contact_iterations = 1
                            if num_iterations > 1:
                                contact_iterations = 5
                            params = {
                                "friction1": friction,
                                "timeintegration": integrator,
                                "damping3": damping,
                                "maxouteriterations": num_iterations,
                                "zonepressurization1": pressurization,
                                "contactiterations": contact_iterations
                            }
                            paramExpressions = {
                                "timestep": str(1.0/fps),
                            }

                            solvernode.setParms(params)
                            solvernode.setParms(params)
                            solvernode.setParmExpressions(paramExpressions)

                            print(f"processing: {integrator}, h={1.0/fps}, d={damping}, n={num_iterations}, f={friction}, p={pressurization}")
                            for i in range(1, num_frames):

                                # Build an output file path used to save geometry
                                #nodefilepath = nodepath.replace("/obj/", "/").replace("/","-")
                                #outfile = projectname + nodefilepath

                                #exp_dir = experiment_dir(fps, integrator, damping, num_iterations, pressurization)

                                #full_out_path = outpath + "/" + exp_dir

                                #if not exists(outpath):
                                #    makedirs(outpath)

                                heights.append(eval(i, node))
                            results += exp_lhs + " = " + str(max(heights)) + "\n"
    nodefilepath = nodepath.replace("/obj/", "/").replace("/","-")
    outfile = outpath + "/" + projectname + nodefilepath + ".txt"
    f = open(outfile, 'w')
    f.write("fps = " + str(fps_l) + "\n")
    f.write("dampings = " + str(damping_l) + "\n")
    f.write("frictions = " + str(friction_l) + "\n")
    f.write("num_iterations = " + str(num_iterations_l) + "\n")
    f.write("pressurizations = " + str(zone_pressurization_l) + "\n")
    f.write("format: <integrator>[<fps>][<damping>][<friction>][<num_iterations>][<zone_pressurization>]\n")
    f.writelines(results)
    f.close()
    print(results)


# If we don't need to generate performance stats, just evaluate the nodes
if args.nohperf:
    run(args)

# Otherwise collect all the needed stats, then run the nodes and finally save the performance stats
else:
    statspath = "stats"
    if not exists(statspath):
        makedirs(statspath)

    # Setup performance monitor
    opts = hou.PerfMonRecordOptions(
            cook_stats = True,
            solve_stats = False,
            draw_stats = False,
            gpu_draw_stats = False,
            viewport_stats = False,
            script_stats = False,
            render_stats = False,
            thread_stats = False,
            frame_stats = False,
            memory_stats = True,
            errors = True)

    if args.subdir != None:
        statsfile = args.subdir
    else:
        statsfile = projectname

    profile = hou.perfMon.startProfile(statsfile, opts)

    run(args)

    profile.stop()

    hperfpath = statspath + "/" + statsfile + ".hperf"

    print("Saving hperf to " + hperfpath)
    profile.save(hperfpath)
